package com.jsite;

import com.jsite.common.idgen.BuildIdFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * @author 13258
 * @description
 * @Create 2019-05-22-10:18
 */
public class RedisIdGeneratorTest {

    public static void main(String[] args) {
        long current = System.currentTimeMillis();
        Set<Long> ids = new HashSet<Long>();
        for (int i = 0; i < 1000; i++) {
            Long id = BuildIdFactory.getInstance().buildFactoryOrderId();
            ids.add(id);
            System.out.println(id);
        }
        System.out.println( "ids >>>>>>" + ids.size());
        System.err.println(System.currentTimeMillis() - current);
    }

}
