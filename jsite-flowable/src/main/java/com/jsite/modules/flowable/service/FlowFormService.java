/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.service;

import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.flowable.dao.FlowFormDao;
import com.jsite.modules.flowable.entity.FlowForm;
import com.jsite.modules.flowable.entity.FormUser;
import com.jsite.modules.flowable.entity.FormVersion;
import com.jsite.modules.flowable.entity.ReProcDef;
import com.jsite.modules.sys.utils.UserUtils;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.impl.persistence.entity.ProcessDefinitionEntityImpl;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.repository.ProcessDefinitionQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程表单生成Service
 * @author liuruijun
 * @version 2019-03-21
 */
@Service
@Transactional(readOnly = true)
public class FlowFormService extends CrudService<FlowFormDao, FlowForm> {

    @Autowired
    private RepositoryService repositoryService;

    @Override
    public FlowForm get(String id) {
        return super.get(id);
    }

    public FlowForm getEntity(FlowForm flowForm) {
        return dao.getEntity(flowForm);
    }

    @Override
    public List<FlowForm> findList(FlowForm flowForm) {
        return super.findList(flowForm);
    }

    @Override
    public Page<FlowForm> findPage(Page<FlowForm> page, FlowForm flowForm) {
        return super.findPage(page, flowForm);
    }

    @Override
    @Transactional(readOnly = false)
    public void save(FlowForm flowForm) {
        super.save(flowForm);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(FlowForm flowForm) {
        super.delete(flowForm);
    }


    @Transactional(readOnly = false)
    public void saveFormUser(FlowForm flowForm) {
        // 更新表单与用户关联
        dao.deleteFormUser(flowForm);
        if (flowForm.getUserList().size() > 0) {
            dao.insertFormUser(flowForm);
        }
    }

    @Transactional(readOnly = false)
    public void saveFormVersion(FormVersion formVersion) {
        dao.deleteFormVersion(formVersion);
        dao.insertFormVersion(formVersion);
    }

    public FormVersion findFormVersion(String procInsId) {
        return dao.findFormVersion(procInsId);
    }

    /**
     * 流程定义列表
     */
    public List<ReProcDef> processList(String category) {
        List<ReProcDef> reProcDefList = new ArrayList<>();

        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery()
                .latestVersion().orderByProcessDefinitionKey().asc();

        if (StringUtils.isNotEmpty(category)){
            processDefinitionQuery.processDefinitionCategory(category);
        }

        List<ProcessDefinition> processDefinitionList = processDefinitionQuery.list();

        List<FormUser> formUserList = dao.findUserForm(UserUtils.getUser().getId());

        for (ProcessDefinition processDefinition : processDefinitionList) {
            for (FormUser formUser : formUserList) {
                if (processDefinition.getKey().equals(formUser.getModelKey())) {
                    ReProcDef reProcDef = new ReProcDef((ProcessDefinitionEntityImpl) processDefinition);
                    reProcDefList.add(reProcDef);
                    break;
                }
            }
        }

        return reProcDefList;
    }

    public List<FormUser> getFlowUserByUserId(String userId){
       return dao.findUserForm(userId);
    }
}