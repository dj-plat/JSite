/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.service;

import com.fasterxml.jackson.databind.JavaType;
import com.jsite.common.mapper.JsonMapper;
import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.flowable.dao.FlowFormAuthorizeDao;
import com.jsite.modules.flowable.entity.FlowForm;
import com.jsite.modules.flowable.entity.TaskNode;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 流程表单生成Service
 * @author liuruijun
 * @version 2019-03-21
 */
@Service
@Transactional(readOnly = true)
public class FlowFormAuthorizeService extends CrudService<FlowFormAuthorizeDao, TaskNode> {

	@Override
    public TaskNode get(String id) {
		return super.get(id);
	}

	@Override
    public List<TaskNode> findList(TaskNode taskNode) {
		return super.findList(taskNode);
	}
	
	@Override
    public Page<TaskNode> findPage(Page<TaskNode> page, TaskNode taskNode) {
		return super.findPage(page, taskNode);
	}
	
	@Override
    @Transactional(readOnly = false)
	public void save(TaskNode taskNode) {
		super.save(taskNode);
	}
	
	@Override
    @Transactional(readOnly = false)
	public void delete(TaskNode taskNode) {
		super.delete(taskNode);
	}

    @Transactional(readOnly = false)
    public void deleteByFieldAndKeyVersion(TaskNode taskNode) {
        dao.deleteByFieldAndKeyVersion(taskNode);
    }

    public List<TaskNode> authorizeNodeList(String modelEditorJson, FlowForm flowForm, String fieldName) {
        JSONObject jsonObject = new JSONObject (modelEditorJson);
        JavaType javaType = JsonMapper.getInstance().createCollectionType(ArrayList.class, TaskNode.class);
        List<TaskNode> sourceList = JsonMapper.getInstance().fromJson(jsonObject.getJSONArray("childShapes").toString(), javaType);
        Iterator<TaskNode> iterator = sourceList.iterator();
        while (iterator.hasNext()) {
            TaskNode node = iterator.next();
            if (!node.getStencil().getId().equals("UserTask") && !node.getStencil().getId().equals("StartNoneEvent")) {
                iterator.remove();
            }
        }
        TaskNode tmp = new TaskNode(fieldName, flowForm.getModelId(), flowForm.getModelKey(), flowForm.getModelVersion());
        List<TaskNode> taskNodeList = this.findList(tmp);
        for (TaskNode sourceNode : sourceList){
            boolean b = false;
            for (TaskNode node : taskNodeList){
                if (node.getResourceId().equals(sourceNode.getResourceId())
                        || node.getResourceId().equals(sourceNode.getProperties().getOverrideid())){
                    node.setResourceName(sourceNode.getProperties().getName());
                    b = true;
                }
            }
            if (!b){
                if (taskNodeList == null) {
                    taskNodeList = new ArrayList<>();
                }
                sourceNode.setModelId(flowForm.getModelId());
                sourceNode.setModelKey(flowForm.getModelKey());
                sourceNode.setModelVersion(flowForm.getModelVersion());
                sourceNode.setResourceId(sourceNode.getProperties().getOverrideid());
                sourceNode.setResourceName(sourceNode.getProperties().getName());
                taskNodeList.add(sourceNode);
            }
        }

        return taskNodeList;
    }

    public List<String> findFieldByKeyVer(String modelKey, String modelVersion) {
        return dao.findFieldByKeyVer(modelKey, modelVersion);
    }

    public List<TaskNode> findTaskNodesByKeyVer(String modelKey, String modelVersion) {
        return dao.findTaskNodesByKeyVer(modelKey, modelVersion);
    }

}